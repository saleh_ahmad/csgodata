<?php

/**
 * Created by PhpStorm.
 * User: oyon
 * Date: 12/2/2016
 * Time: 12:27 PM
 */
class Config_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getValue($slug = false)
    {
        if (!$slug) {
            throw new Exception('Key Not Found');
        }

        $query = $this->db->get_where('config', ['key_name' => $slug]);
        return $query->row()->value;
    }
}