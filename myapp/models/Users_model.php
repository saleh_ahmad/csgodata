<?php

/**
 * Created by PhpStorm.
 * User: oyon
 * Date: 12/8/2016
 * Time: 9:42 PM
 */
class Users_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function saveUserInformation($data)
    {
        extract($data);

        if (!$steamId) {
            throw new Exception('Steam ID Not Found');
        }

        $query = $this->db->get_where('users', [
            'steam64_id' => $steamId
        ]);

        date_default_timezone_set('Asia/Dhaka');
        $date = date("Y-m-d H:i:s");

        if (!$query->num_rows()) {
            $value = [
                'steam64_id'        => $steamId,
                'last_ip'           => $ip,
                'last_login'        => $date,
                'last_used_browser' => $browser
            ];

            $this->db->insert('users', $value);
            return;
        }

        $value = [
            'last_ip'           => $ip,
            'last_login'        => $date,
            'last_used_browser' => $browser
        ];
        $this->db->where('steam64_id', $steamId);
        $this->db->update('users', $value);
    }
}