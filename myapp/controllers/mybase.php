<?php
class MyBase extends CI_Controller
{
    public $apiKey = false;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Config_model');
        $this->load->model('Users_model');

        $this->apiKey = $this->Config_model->getValue('api_key');
    }

    public function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        /** Optional Authentication: */
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (!curl_errno($curl)) {
            $result = json_decode($result);
            $result->statuscode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $result = json_encode($result);
        }

        curl_close($curl);
        return $result;
    }

    private function getIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    private function getBrowserName($user_agent)
    {
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) {
            return 'Opera';
        } elseif (strpos($user_agent, 'Edge')) {
            return 'Edge';
        } elseif (strpos($user_agent, 'Chrome')) {
            return 'Chrome';
        } elseif (strpos($user_agent, 'Safari')) {
            return 'Safari';
        } elseif (strpos($user_agent, 'Firefox')) {
            return 'Firefox';
        } elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) {
            return 'Internet Explorer';
        }
        return 'Other';
    }

    protected function saveUser($steamID)
    {
        $data['steamId'] = $steamID;
        $data['ip']      = $this->getIp();
        $data['browser'] = $this->getBrowserName($_SERVER['HTTP_USER_AGENT']).' : '.$_SERVER['HTTP_USER_AGENT'];

        $this->Users_model->saveUserInformation($data);
    }
}