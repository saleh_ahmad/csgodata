<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');
include_once  APPPATH.'/controllers/mybase.php';

class Items extends MyBase
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index($id, $user)
    {
        if (! file_exists(APPPATH.'views/items.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title']      = 'User Inventory';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';

        try {
            $url = $this->CallAPI('GET', 'http://steamcommunity.com/'.$id.'/'.$user.'/inventory/json/730/2');
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);
            if(count($content)) {
                $data['all'] = $content->rgDescriptions;
            }
        } catch (Exception $e) {
            $data['all'] = [];
            $data['msg']  = $e->getMessage();
        }

        $this->load->view('templates/header', $data);
        $this->load->view('items', $data);
        $this->load->view('templates/footer');
    }

    public function details($id = false)
    {
        if (! file_exists(APPPATH.'views/itemdetails.php')) {
            show_404();
        }

        if (!$id) {
            throw new Exception('Item ID needed');
        }

        $data['title']  = 'Item details';
        $data['search'] = false;

        try {
            $url = $this->CallAPI('GET', 'http://steamcommunity.com/market/priceoverview/?currency=USD&appid=730&market_hash_name='.$id);
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);
            $data['details'] = $content;
        } catch (Exception $e) {
            $data['details'] = [];
            $data['msg']  = $e->getMessage();
        }

        $data['stat'] = $this->stat($id);

        $this->load->view('templates/header', $data);
        $this->load->view('itemdetails', $data);
        $this->load->view('templates/footer');
    }

    private function stat($id = false)
    {
        if (!$id) {
            throw new Exception('Item ID needed');
        }

        try {
            $url = $this->CallAPI('GET', 'http://csgobackpack.net/api/GetItemPrice/?currency=USD&id='.$id.'&time=7&icon=1');
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);
            $data['details'] = $content;
        } catch (Exception $e) {
            $data['details'] = [];
            $data['msg']  = $e->getMessage();
        }

        return $data;
    }
}