<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'controllers/mybase.php';

class Game extends MyBase
{
    public function __construct()
    {
        parent::__construct();
    }

    private function fetchGameInformation($id)
    {
        try {
            $url = $this->CallAPI('GET', 'http://store.steampowered.com/api/appdetails?appids='.$id);
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);
            $data['gameinfo'] = $content->$id;
        } catch (Exception $e) {
            $data['gameinfo'] = [];
            $data['msg']  = $e->getMessage();
        }
        return $data;
    }

    public function info($id)
    {
        if (!file_exists(APPPATH.'views/gameinfo.php')) {
            show_404();
        }
        $data = $this->fetchGameInformation($id);
        $data['title']      = $data['gameinfo']->data->name;
        $data['search']     = true;
        $data['searchtext'] = 'App ID';

        $this->load->view('templates/header', $data);
        $this->load->view('gameinfo', $data);
        $this->load->view('templates/footer');
    }

    public function screenshots($id)
    {
        if (!file_exists(APPPATH.'views/gamescreenshots.php')) {
            show_404();
        }
        $data = $this->fetchGameInformation($id);
        $data['title']       = 'Game Screenshots';
        $data['search']      = true;
        $data['searchtext']  = 'App ID';
        $data['screenshots'] = $data['gameinfo']->data->screenshots;

        $this->load->view('templates/header', $data);
        $this->load->view('gamescreenshots', $data);
        $this->load->view('templates/footer');
    }

    public function movies($id)
    {
        if (!file_exists(APPPATH.'views/gamemovies.php')) {
            show_404();
        }
        $data = $this->fetchGameInformation($id);
        $data['title']      = 'Game Movies';
        $data['search']     = true;
        $data['searchtext'] = 'App ID';
        $data['movies']     = $data['gameinfo']->data->movies;

        $this->load->view('templates/header', $data);
        $this->load->view('gamemovies', $data);
        $this->load->view('templates/footer');
    }

    public function userstats($appid, $steam64)
    {
        if (!file_exists(APPPATH.'views/userstats.php')) {
            show_404();
        }
        $data['title']      = 'User Stats';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';

        try {
            $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid='.$appid.'&key='.$this->apiKey.'&steamid='.$steam64);
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);
            $data['userstats'] = $content->playerstats;

            foreach ($content->playerstats->stats as $stat) {
                $data['statistics'][$stat->name] = $stat->value;
            }

            $this->load->view('templates/header', $data);
            $this->load->view('userstats', $data);
            $this->load->view('templates/footer');
        } catch (Exception $e) {
            $data['userstats'] = [];
            $data['msg']  = $e->getMessage();
        }
    }
}