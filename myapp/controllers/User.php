<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'/controllers/mybase.php';
include_once APPPATH.'helpers/country.php';

class User extends MyBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function profile($id = false)
    {
        if (! file_exists(APPPATH.'views/home.php')) {
            show_404();
        }

        $data = $this->fetch_info($id);

        $_SESSION['url']         = $data['info']->profileurl;
        $_SESSION['avatar']      = $data['info']->avatar;
        $_SESSION['personaname'] = $data['info']->personaname;

        $data['id']     = $id;
        $data['title']  = 'Welcome';
        $data['search'] = false;

        $this->load->view('templates/header', $data);
        $this->load->view('home', $data);
        $this->load->view('templates/footer');
    }

    public function info($id = false)
    {
        if (! file_exists(APPPATH.'views/information.php')) {
            show_404();
        }

        $data = $this->fetch_info($id);

        $data['title']      = 'User Information';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';
        $data['country']    = isset($data['info']->loccountrycode)
            ? (new Country())->getFullCountry($data['info']->loccountrycode)
            : '';

        $this->load->view('templates/header', $data);
        $this->load->view('information', $data);
        $this->load->view('templates/footer');
    }

    private function fetch_info($id = false)
    {
        if (!$id) {
            throw new Exception('Steam64 ID needed');
        }

        $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.$this->apiKey.'&steamids='.$id);
        $content = json_decode($url);

        $data = [];
        if ($content->statuscode == 200) {
            $data['info'] = $content->response->players[0];
        } elseif ($content->statuscode == 429) {
            $data['msg'] = 'An error occurred. Please try after sometime!';
        }

        return $data;
    }
    
    public function friends($id)
    {
        if (! file_exists(APPPATH.'views/friends.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title']      = 'Friend List';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';


        $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key='.$this->apiKey.'&steamid='.$id.'&relationship=friend');
        $content = json_decode($url);

        $data['friends']= [];
        if ($content->statuscode == 200) {
            $data['friends'] = $content->friendslist->friends;
        } elseif ($content->statuscode == 429) {
            $data['msg'] = 'An error occurred. Please try after sometime!';
        }

        $this->load->view('templates/header', $data);
        $this->load->view('friends', $data);
        $this->load->view('templates/footer');
    }

    public function getOwnedGames($id)
    {
        if (! file_exists(APPPATH.'views/getOwnedGames.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title']      = 'Owned Games';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';

        $url = $this->CallAPI('GET', 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key='.$this->apiKey.'&steamid='.$id.'&format=json');
        $content = json_decode($url);

        $data['owned'] = [];
        if ($content->statuscode == 200) {
            $data['owned'] = $content->response;
        } elseif ($content->statuscode == 429) {
            $data['msg'] = 'An error occurred. Please try after sometime!';
        }
        
        $this->load->view('templates/header', $data);
        $this->load->view('getOwnedGames', $data);
        $this->load->view('templates/footer');
    }

    public function recentlyPlayedGames($id)
    {
        if (! file_exists(APPPATH.'views/recentlyPlayedGames.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title']      = 'Recently Played Games';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';

        $url = $this->CallAPI('GET', 'http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key='.$this->apiKey.'&steamid='.$id.'&format=json');
        $content = json_decode($url);

        $data['recPlayed'] = [];
        if ($content->statuscode == 200) {
            $data['recPlayed'] = $content->response;
        } elseif ($content->statuscode == 429) {
            $data['msg'] = 'An error occurred. Please try after sometime!';
        }

        $this->load->view('templates/header', $data);
        $this->load->view('recentlyPlayedGames', $data);
        $this->load->view('templates/footer');
    }

    public function bans($id)
    {
        if (! file_exists(APPPATH.'views/bans.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        
        $data['title']      = 'User Bans';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';

        $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUser/GetPlayerBans/v1/?key='.$this->apiKey.'&steamids='.$id);
        $content = json_decode($url);

        $data['bans'] = [];
        if ($content->statuscode == 200) {
            $data['bans'] = $content->players[0];
        } elseif ($content->statuscode == 429) {
            $data['msg'] = 'An error occurred. Please try after sometime!';
        }

        $this->load->view('templates/header', $data);
        $this->load->view('bans', $data);
        $this->load->view('templates/footer');
    }
    
    public function logout()
    {
        session_unset();
        session_destroy();
        header('Location: '.base_url(''));
    }
}