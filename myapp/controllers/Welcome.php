<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/openid.php';
include_once APPPATH.'controllers/mybase.php';

class Welcome extends MyBase
{
    public function __construct()
    {
        parent::__construct();
    }
	public function index()
	{
        if (! file_exists(APPPATH.'views/welcome_message.php')) {
			show_404();
		}

        if (isset($_SESSION['steam64'])) {
            redirect(base_url('profile/'.$_SESSION['steam64']));
        }

		try {
			$openid = new LightOpenID(base_url());

			if (!$openid->mode) {
				if (isset($_GET['login'])) {
					$openid->identity = 'http://steamcommunity.com/openid';
					redirect($openid->authUrl());
				}

				$data['title'] = 'Welcome';

                $this->load->view('templates/header', $data);
                $this->load->view('welcome_message');
                $this->load->view('templates/footer');
			} elseif ($openid->mode == 'cancel') {
				echo 'User has canceled authentication!';
			} else {
				if ($openid->validate()) {
					$id = $openid->identity;
					$ptn = "/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
					preg_match($ptn, $id, $matches);

                    $_SESSION['steam64'] = $matches[1];
                    $this->saveUser($_SESSION['steam64']);

                    redirect(base_url('profile/'.$matches[1]));
				} else {
					echo "User is not logged in.\n";
				}
			}
		} catch (ErrorException $e) {
			echo $e->getMessage();
		}
	}
}
