<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'controllers/mybase.php';

class Csgo extends MyBase
{
    public function __construct()
    {
        parent::__construct();
    }

    private function fetchWeaponData($steam64 = false)
    {
        if (!$steam64) {
            throw new Exception('Steam64 ID needed');
        }

        $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key='.$this->apiKey.'&steamid='.$steam64);
        $content = json_decode($url);

        $data = [];
        if ($content->statuscode == 200) {
            foreach ($content->playerstats->stats as $stat) {
                $data['weaponstats'][$stat->name] = $stat->value;
            }
        }

        return $data;
    }

    public function operationstat($id, $user)
    {
        if (!file_exists(APPPATH.'views/operationstats.php')) {
            show_404();
        }

        $data['title']      = 'Operation Statistics';
        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';

        $url = $this->CallAPI('GET', 'http://steamcommunity.com/'.$id.'/'.$user.'/inventory/json/730/2');
        $content = json_decode($url);

        $data['operation'] = [];
        if ($content->statuscode == 200) {
            if (count($content)) {
                foreach ($content->rgDescriptions as $i => $item) {
                    $coin = explode(' ', $item->name);
                    $lastElement = array_slice($coin, -1)[0];

                    if ($lastElement == 'Coin') {
                        $data['operation'][] = $content->rgDescriptions->$i;
                    }
                }
            }
        } elseif ($content->statuscode == 429) {
            $data['msg'] = 'An error occurred. Please try after sometime!';
        }

        $this->load->view('templates/header', $data);
        $this->load->view('operationstats', $data);
        $this->load->view('templates/footer');
    }

    public function weaponstat($steam64)
    {
        if (!file_exists(APPPATH.'views/weaponstats.php')) {
            show_404();
        }

        if (!$steam64) {
            throw new Exception('Steam64 ID needed');
        }

        $data = $this->fetchWeaponData($steam64);

        $data['title'] = 'Weapon Statistics';
        $weapons = ['glock', 'deagle', 'elite', 'fiveseven', 'xm1014', 'mac10', 'ump45', 'p90', 'awp', 'ak47', 'aug',
            'famas', 'g3sg1', 'm249', 'p250', 'hkp2000', 'sg556', 'scar20', 'ssg08', 'mp7', 'mp9', 'nova', 'negev',
            'sawedoff', 'bizon', 'tec9', 'mag7', 'm4a1', 'galilar', 'knife', 'hegrenade', 'molotov', 'taser'];

        $data['search']     = true;
        $data['searchtext'] = 'Steam64 ID';
        $data['weapondata'] = $this->weaponlist($data['weaponstats'], $weapons);

        $this->load->view('templates/header', $data);
        $this->load->view('weaponstats', $data);
        $this->load->view('templates/footer');
    }

    public function weapon($type, $steam64)
    {
        if (!file_exists(APPPATH.'views/weaponstats.php')) {
            show_404();
        }

        if (!$type) {
            throw new Exception('Weapon Type needed');
        }

        if (!$steam64) {
            throw new Exception('Steam64 ID needed');
        }

        $data = $this->fetchWeaponData($steam64);

        switch ($type) {
            case 'pistol':
                $data['title'] = 'Pistol Statistics';
                $weapons = ['glock', 'deagle', 'elite', 'fiveseven', 'p250', 'hkp2000', 'tec9'];
                break;
            case 'rifle':
                $data['title'] = 'Rifle Statistics';
                $weapons = ['awp', 'ak47', 'aug', 'famas', 'g3sg1', 'sg556', 'scar20', 'ssg08', 'm4a1', 'galilar'];
                break;
            case 'smg':
                $data['title'] = 'SMG Statistics';
                $weapons = ['mac10', 'ump45', 'p90', 'mp7', 'mp9', 'bizon'];
                break;
            case 'heavy':
                $data['title'] = 'Heavy Statistics';
                $weapons = ['xm1014', 'm249', 'nova', 'negev', 'sawedoff', 'mag7'];
                break;
            case 'others':
                $data['title'] = 'Other Weapons Statistics';
                $weapons = ['knife', 'hegrenade', 'molotov', 'taser'];
                break;
            default:
                $weapons = ['glock', 'deagle', 'elite', 'fiveseven', 'xm1014', 'mac10', 'ump45', 'p90', 'awp', 'ak47', 'aug',
                    'famas', 'g3sg1', 'm249', 'p250', 'hkp2000', 'sg556', 'scar20', 'ssg08', 'mp7', 'mp9', 'nova', 'negev',
                    'sawedoff', 'bizon', 'tec9', 'mag7', 'm4a1', 'galilar', 'knife', 'hegrenade', 'molotov', 'taser'];
        }

        $data['weapondata'] = $this->weaponlist($data['weaponstats'], $weapons);

        echo json_encode($data);
    }



    private function weaponlist($data, $weapons)
    {
        $newdata = [];

        foreach ($weapons as $w) {
            switch ($w) {
                case 'knife':
                case 'hegrenade':
                case 'molotov':
                case 'taser':
                    $newdata[$w] = [
                        'kills'    => $data['total_kills_'.$w],
                    ];
                    break;
                default:
                    $newdata[$w] = [
                        'kills'    => $data['total_kills_'.$w],
                        'hits'     => $data['total_hits_'.$w],
                        'shots'    => $data['total_shots_'.$w],
                        'accuracy' => ($data['total_hits_'.$w] / $data['total_shots_'.$w]) * 100
                    ];
            }
        }
        return $newdata;
    }

    public function mapstat($steam64)
    {
        if (!file_exists(APPPATH.'views/mapstats.php')) {
            show_404();
        }

        if (!$steam64) {
            throw new Exception('Steam64 ID needed');
        }

        try {
            $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key='.$this->apiKey.'&steamid='.$steam64);
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);

            foreach ($content->playerstats->stats as $stat) {
                $data['statistics'][$stat->name] = $stat->value;
            }

            $maps = ['cs_assault', 'cs_italy', 'cs_office', 'de_aztec', 'de_cbble', 'de_dust2', 'de_dust', 'de_inferno',
            'de_nuke', 'de_train', 'de_lake', 'de_safehouse', 'de_sugarcane', 'de_stmarc', 'de_bank', 'de_shorttrain',
            'ar_shoots', 'ar_baggage', 'ar_monastery', 'de_vertigo', 'cs_militia'];

            $data['mapdata']    = $this->maplist($data['statistics'], $maps);
            $data['title']      = 'Maps Statistics';
            $data['search']     = true;
            $data['searchtext'] = 'Steam64 ID';

            $this->load->view('templates/header', $data);
            $this->load->view('mapstats', $data);
            $this->load->view('templates/footer');
        } catch (Exception $e) {
            $data['mapdata'] = [];
            $data['msg']  = $e->getMessage();
        }
    }

    private function maplist($data, $maps)
    {
        $newdata = [];

        foreach ($maps as $m) {
            $newdata[$m] = [
                'wins'           => isset($data['total_wins_map_'.$m]) ? $data['total_wins_map_'.$m] : '',
                'rounds'         => isset($data['total_rounds_map_'.$m]) ? $data['total_rounds_map_'.$m] : '',
                'win_percentage' => (isset($data['total_wins_map_'.$m]) && isset($data['total_rounds_map_'.$m]))
                                    ? ($data['total_wins_map_'.$m] / $data['total_rounds_map_'.$m]) * 100
                                    : NULL
            ];
        }
        return $newdata;
    }


    public function achievements($steam64)
    {
        if (!file_exists(APPPATH.'views/achievements.php')) {
            show_404();
        }

        if (!$steam64) {
            throw new Exception('Steam64 ID needed');
        }

        try {
            $url = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid=730&key='.$this->apiKey.'&steamid='.$steam64);
            if (!$url) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url);
            $data['userachievements'] = $content->playerstats->achievements;

            $url2 = $this->CallAPI('GET', 'http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key='.$this->apiKey.'&appid=730');
            if (!$url2) {
                throw new Exception('An Error Occurred!! Please try after sometime.');
            }
            $content = json_decode($url2);
            $data['achievementschema'] = $content->game->availableGameStats->achievements;

            $data['title']      = 'Achievements';
            $data['search']     = true;
            $data['searchtext'] = 'Steam64 ID';

            $this->load->view('templates/header', $data);
            $this->load->view('achievements', $data);
            $this->load->view('templates/footer');
        } catch (Exception $e) {
            $data['userachievements'] = [];
            $data['msg']  = $e->getMessage();
        }
    }
}