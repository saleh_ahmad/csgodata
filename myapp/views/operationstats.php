<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
    <?php if (isset($msg)): ?>
        <div class="center">
            <?= heading($msg, 3); ?>
        </div>
    <?php elseif(!count($operation)): ?>
        <div class="center">
            <?= heading('No Operation Statistics Found!', 3); ?>
        </div>
    <?php else: ?>
        <div class="center">
            <?= heading($title, 3); ?>
        </div>

        <div class="mdl-grid">
            <?php foreach($operation as $i => $item): ?>
                <?php $item->name = str_replace([' Challenge', ' Coin', 'Gold'], '', $item->name); ?>
                <div class="mdl-cell mdl-cell--4-col mdl-cell--6-col-tablet mdl-cell--12-col-phone">
                    <div class="center">
                        <?= heading($item->name, 4); ?>
                    </div>
                    <table class="mdl-data-table mdl-js-data-table center">
                        <tbody>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric" colspan="2">
                                <?= img([
                                    'src'   => 'http://steamcommunity-a.akamaihd.net/economy/image/'.$item->icon_url.'/330x192',
                                    'alt'   => $item->name,
                                    'class' => 'img-center'
                                ]); ?>
                            </td>
                        </tr>

                        <?php $desc = []; foreach($item->descriptions as $des): ?>
                            <?php if (strpos($des->value, ':') !== false): ?>
                                <tr>
                                    <td>
                                        <?= explode(': ', $des->value)[0]; ?>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <?= explode(': ', $des->value)[1] ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</section>
<!--<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/csgo/weaponstat/<?/*= $type; */?>/'+id;
        }
    });
</script>-->