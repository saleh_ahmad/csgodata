<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .cover-image-map{
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        z-index: -1;
        background-image: url(<?= base_url('assets/img/map-background.jpg') ?>);
        background-position: center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        filter: blur(3px);
        height: 94.4%;
        width: 99%;
    }
</style>
<section>
    <div class="cover-image-map"></div>
    <?php if(!count($mapdata)): ?>
        <h1><?= $msg; ?></h1>
    <?php else: ?>
        <div class="center">
            <?= heading($title, 3); ?>
        </div>
    <div class="mdl-grid center">
        <?php foreach($mapdata as $i => $data): ?>
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--6-col-phone">
                <?= heading($i, 4); ?>
                <div class="map-card">
                    <?= img([
                        'src'   => base_url('assets/img/maps/'.$i.'.jpg'),
                        'class' => 'width-100',
                        'alt'   => $i
                    ]); ?>
                    <?php if (file_exists('assets/img/maps/logo/'.$i.'.png')): ?>
                    <?= img([
                        'src'   => base_url('assets/img/maps/logo/'.$i.'.png'),
                        'class' => 'map-logo',
                        'alt'   => $i
                    ]); ?>
                    <?php endif; ?>
                </div>
                <table class="mdl-data-table mdl-js-data-table width-100">
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Rounds Won
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $data['wins'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Rounds Played
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $data['rounds'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Win
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= number_format($data['win_percentage'], 2, '.', '').' %'; ?>
                        </td>
                    </tr>
                </table>
            </div>
        <?php endforeach;; ?>
    </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/csgo/mapstat/'+id;
        }
    });
</script>