<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .cover-image-map{
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        z-index: -1;
        background-image: url(<?= base_url('assets/img/W_2013_232.jpg') ?>);
        background-position: center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        filter: blur(3px);
        height: 94.4%;
        width: 99%;
    }
</style>
<section>
    <div class="cover-image-map"></div>
    <?php $totalAchievements = count($userachievements);
    if(!$totalAchievements): ?>
        <h1><?= $msg; ?></h1>
    <?php else: ?>
    <div class="mdl-grid center">
        <?php $achievementUnlocked = array_count_values(array_map(function($foo){return $foo->achieved;}, $userachievements))['1'] ?>
        <style>
            .mdl-progress>.progressbar {
                <?php if ($achievementUnlocked > 140): ?>
                background-color: #0eb114;
                <?php elseif ($achievementUnlocked > 120 && $achievementUnlocked < 140): ?>
                background-color: #a812c3;
                <?php elseif ($achievementUnlocked > 80 && $achievementUnlocked < 120): ?>
                background-color: rgb(255,64,129);
                <?php elseif ($achievementUnlocked > 40 && $achievementUnlocked < 80): ?>
                background-color: #ff9800;
                <?php else: ?>
                background-color: #d50000;
                <?php endif; ?>
            }
        </style>
        <div class="mdl-cell mdl-cell--4-col">
            <?= heading('Unlocked Achievements: '.$achievementUnlocked.'/'.$totalAchievements, 5); ?>
        </div>
        <div class="mdl-cell mdl-cell--8-col">
            <div id="achievements-progress" class="mdl-progress mdl-js-progress" style="margin-top: 32px;"></div>
            <script>
                document.querySelector('#achievements-progress').addEventListener('mdl-componentupgraded', function() {
                    this.MaterialProgress.setProgress(<?= $achievementUnlocked/$totalAchievements*100 ?>);
                });
            </script>
        </div>
        <div class="mdl-cell mdl-cell--12-col">
            <table class="mdl-data-table mdl-js-data-table center">
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">Name</th>
                    <th></th>
                    <th class="mdl-data-table__cell--non-numeric">Description</th>
                    <th class="mdl-data-table__cell--non-numeric">Status</th>
                </tr>
                <?php foreach ($userachievements as $i => $ua): ?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?= $ua->apiname; ?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php
                        if ($ua->achieved) {
                            $check = [
                                'src'   => $achievementschema[$i]->icon,
                                'alt'   => 'Done',
                                'title' => $achievementschema[$i]->displayName
                            ];
                        } else {
                            $check = [
                                'src'   => $achievementschema[$i]->icongray,
                                'alt'   => 'Not Done',
                                'title' => $achievementschema[$i]->displayName
                            ];
                        }
                        echo img($check);
                        ?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?= $achievementschema[$i]->description; ?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php
                        if ($ua->achieved) {
                            $check2 = [
                                'src'   => base_url('assets/img/check-mark.png'),
                                'alt'   => 'Done'
                            ];
                        } else {
                            $check2 = [
                                'src'   => base_url('assets/img/X_mark.png'),
                                'alt'   => 'Done'
                            ];
                        }
                        echo img($check2);
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/csgo/achievements/'+id;
        }
    });
</script>