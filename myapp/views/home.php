<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
    <?php if(!count($info)): ?>
        <div class="center">
            <?= heading('No Information Found!', 2); ?>
        </div>
    <?php else: ?>
    <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--12-col">
            <marquee style="color:#fff">
                **User stats in game only available for Counter-Strike: Global Offensive (App ID: 730)
            </marquee>
            <?= heading('Welcome '.$info->personaname.'!', 3); ?>
            <?php
            $avatarfull = [
                'src' => $info->avatarfull,
                'alt' => 'User Avatar Full'
            ];
            echo img($avatarfull);
            ?>
        </div>
    </div>
    <?php endif; ?>
</section>