<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if(!count($screenshots)): ?>
        <h1><?= $msg; ?></h1>
    <?php else: ?>
        <div class="center">
            <?= heading($gameinfo->data->name." : Screenshots", 3); ?>
        </div>
        <div class="mdl-grid">
            <?php foreach($screenshots as $item): ?>
            <div class="mdl-cell mdl-cell--6-col">
                <?=
                anchor(
                    $item->path_full,
                    img([
                        'src'   => $item->path_thumbnail,
                        'class' => 'width-100',
                        'alt'   => 'Screenshot_'.$item->id
                    ]),
                    $att = ['target' => '_blank']);
                ?>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/game/screenshots/'+id;
        }
    });
</script>