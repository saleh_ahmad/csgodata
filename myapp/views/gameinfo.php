<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if(!count($gameinfo)): ?>
        <h1><?= $msg; ?></h1>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?= heading($gameinfo->data->name, 3); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <tr>
                        <td>Type</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->type ?></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->name ?></td>
                    </tr>
                    <?php if($gameinfo->data->steam_appid == 730): ?>
                    <tr>
                        <td>User Stats</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor(base_url('game/userstats/'.$gameinfo->data->steam_appid.'/'.$_SESSION['steam64']), 'Check User stats for this game', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Steam App ID</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->steam_appid ?></td>
                    </tr>
                    <tr>
                        <td>Required Age</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->required_age ?></td>
                    </tr>
                    <tr>
                        <td>Free</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php if($gameinfo->data->is_free): ?>
                                YES
                            <?php else: ?>
                                NO
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php if(isset($gameinfo->data->controller_support)): ?>
                    <tr>
                        <td>Controller Support</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->controller_support ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Detailed Description</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->detailed_description ?></td>
                    </tr>
                    <tr>
                        <td>Short Description</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->short_description ?></td>
                    </tr>
                    <tr>
                        <td>Supported Languages</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->supported_languages ?></td>
                    </tr>
                    <tr>
                        <td>Header Image</td>
                        <td class="mdl-data-table__cell--non-numeric"><img src="<?= $gameinfo->data->header_image ?>" alt="Header Image" /></td>
                    </tr>
                    <tr>
                        <td>Website</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor($gameinfo->data->website, $gameinfo->data->website, $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Requirements for Windows</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->pc_requirements->minimum; ?></td>
                    </tr>
                    <?php if(isset($gameinfo->data->mac_requirements->minimum)): ?>
                    <tr>
                        <td>Requirements for Mac</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->mac_requirements->minimum; ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if(isset($gameinfo->data->linux_requirements->minimum)): ?>
                    <tr>
                        <td>Requirements for Linux</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $gameinfo->data->linux_requirements->minimum; ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Developers</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php foreach($gameinfo->data->developers as $i => $d): ?>
                                <?= $d; ?><br/>
                            <?php endforeach;?>
                        </td>
                    </tr>
                    <tr>
                        <td>Publishers</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php foreach($gameinfo->data->publishers as $i => $d): ?>
                                <?= $d; ?><br/>
                            <?php endforeach;?>
                        </td>
                    </tr>
                    <?php if(isset($gameinfo->data->price_overview)): ?>
                    <tr>
                        <td rowspan="4">Price Overview</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= 'Currency: '.$gameinfo->data->price_overview->currency ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric"><?= 'Initial: $'.$gameinfo->data->price_overview->initial/100; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric"><?= 'Final: $'.$gameinfo->data->price_overview->final/100; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric"><?= 'Discount percentage: '.$gameinfo->data->price_overview->discount_percent; ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if(isset($gameinfo->data->packages)): ?>
                    <tr>
                        <td>Packages</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php foreach($gameinfo->data->packages as $i => $p): ?>
                                <?= $p; ?><br/>
                            <?php endforeach;?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <!--<tr>
                        <td>Package Groups</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php
/*                            echo '<pre>';
                            var_dump($gameinfo->data->package_groups);
                            echo '</pre>';
                            */?>
                        </td>
                    </tr>-->
                    <tr>
                        <td>Platorms</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <ul>
                                <li>
                                    Windows: <?= $gameinfo->data->platforms->windows ? 'YES<br/>' : 'NO<br/>'; ?>
                                </li>
                                <li>
                                    MAC: <?= $gameinfo->data->platforms->mac ? 'YES<br/>' : 'NO<br/>'; ?>
                                </li>
                                <li>
                                    Linux: <?= $gameinfo->data->platforms->linux ? 'YES<br/>' : 'NO<br/>'; ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <?php if(isset($gameinfo->data->metacritic)): ?>
                    <tr>
                        <td>Metacritic</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <b>Score: </b><?= $gameinfo->data->metacritic->score ?><br />
                            <b>Url: </b><?= anchor($gameinfo->data->metacritic->url, $gameinfo->data->metacritic->url, $attr = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Categories</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php foreach ($gameinfo->data->categories as $cat): ?>
                                <span class="mdl-chip">
                                    <span class="mdl-chip__text"><?= $cat->description; ?></span>
                                </span>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Genres</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $gameinfo->data->genres[0]->description ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Screenshots</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor(base_url('game/screenshots/'.$gameinfo->data->steam_appid), 'See Screenshots', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <?php if(isset($gameinfo->data->movies)): ?>
                    <tr>
                        <td>Movies</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor(base_url('game/movies/'.$gameinfo->data->steam_appid), 'See Movies', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Recommendations</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $gameinfo->data->recommendations->total; ?>
                        </td>
                    </tr>
                    <?php if(isset($gameinfo->data->achievements)): ?>
                    <tr>
                        <td>Achievements</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <b>Total Achievements: </b><?= $gameinfo->data->achievements->total ?><br /><br />
                            <b>Highlighted Achievements: </b><br />
                            <?php foreach ($gameinfo->data->achievements->highlighted as $ac): ?>
                                <?php
                                $attr= [
                                    'src'   => $ac->path,
                                    'alt'   => $ac->name,
                                    'title' => $ac->name
                                ];
                                echo img($attr);
                                ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Release Date:</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $gameinfo->data->release_date->date; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Support Info</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            URL: <?php if ($gameinfo->data->support_info->url){ echo anchor($gameinfo->data->support_info->url, $gameinfo->data->support_info->url, $att = ['target' => '_blank']); } ?><br/><br/>
                            Email: <?= $gameinfo->data->support_info->email ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Background</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <img src="<?= $gameinfo->data->background; ?>" alt="Background" style="width: 100%" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/game/info/'+id;
        }
    });
</script>