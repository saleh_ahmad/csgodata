<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if (!isset($all) || !count($all)): ?>
        <div class="center">
            <h2>No items found.</h2>
        </div>
    <?php else: ?>
        <div class="center">
            <?= heading('Counter Strike Global Offensive', 3); ?>
        </div>
        <div class="mdl-grid center">
            <?php foreach($all as $i => $item): ?>
            <div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--12-col-phone">
                <?= heading('ID: '.$i, 4); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                <tbody>
                <tr>
                    <td>Name:</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $item->name; ?></td>
                </tr>
                <tr>
                    <td>Image:</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?= img([
                            'src'   => 'http://steamcommunity-a.akamaihd.net/economy/image/'.$item->icon_url.'/330x192',
                            'alt'   => $item->name
                        ]); ?>
                    </td>
                </tr>
                <tr>
                    <td>Market Name:</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $item->market_name; ?></td>
                </tr>
                <tr>
                    <td>Market Hash Name:</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?= anchor(
                            base_url('items/details/'.str_replace('|', '%7C', str_replace('\u2122', '™', str_replace(' ', '%20', $item->market_hash_name)))),
                            $item->market_hash_name,
                            $att = ['target' => '_blank']
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td>Check on Community Market:</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?= anchor('http://steamcommunity.com/market/listings/730/'.str_replace('|', '%7C', str_replace('\u2122', '™', str_replace(' ', '%20', $item->market_hash_name))), $item->market_hash_name, $att = ['target' => '_blank']); ?>
                    </td>
                </tr>
                <tr>
                    <td>Type:</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $item->type; ?></td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php foreach($item->descriptions as $des): ?>
                            <div <?php if(isset($des->color)){echo "style='color:#$des->color';";}?>><?= $des->value; ?></div><br/>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <?php if(isset($item->actions)): ?>
                    <tr>
                        <td>Actions:</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php foreach($item->actions as $a): ?>
                                <a href="<?= $a->link; ?>" target="_blank"><?= $a->name; ?></a>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>Tags:</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php $tags=''; foreach($item->tags as $t): ?>
                            <?php $tags.=$t->name.', ' ?>
                        <?php endforeach; echo rtrim($tags, ', ') ?>
                    </td>
                </tr>
                </tbody>
            </table>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</section>