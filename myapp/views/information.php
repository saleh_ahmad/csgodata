<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if (isset($msg)): ?>
        <div class="center">
            <?= heading($msg, 3); ?>
        </div>
    <?php elseif (!count($info)): ?>
        <div class="center">
            <?= heading('No Information Found!', 2); ?>
        </div>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?= heading('Basic Information', 3); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <tbody>
                    <tr>
                        <td>Steam ID</td>
                        <!-- 64bit SteamID of the user -->
                        <td class="mdl-data-table__cell--non-numeric"><?= $info->steamid; ?></td>
                    </tr>

                    <tr>
                        <td>Community Visibility State</td>
                        <!--This represents whether the profile is visible or not, and if it is visible, why you are allowed to see it.
                        Note that because this WebAPI does not use authentication, there are only two possible values returned:
                        1 - the profile is not visible to you (Private, Friends Only, etc),
                        3 - the profile is "Public", and the data is visible.
                        Mike Blaszczak's post on Steam forums says, "The community visibility state this API returns is different than
                        the privacy state. It's the effective visibility state from the account making the request to the account being
                        viewed given the requesting account's relationship to the viewed account."-->
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php
                            switch ($info->communityvisibilitystate) {
                                case 1:
                                    echo 'Not Visible';
                                    break;
                                case 3:
                                    echo 'Visible';
                                    break;
                            }
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Profile State</td>
                        <!--If set, indicates the user has a community profile configured (will be set to '1')-->
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= isset($info->profilestate) ? $info->profilestate : ''; ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Personal Name</td>
                        <!-- The player's persona name (display name) -->
                        <td class="mdl-data-table__cell--non-numeric"><?= $info->personaname; ?></td>
                    </tr>

                    <tr>
                        <td>Last Log Off</td>
                        <!--The last time the user was online, in unix time.-->
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= gmdate("F j, Y, g:i a", $info->lastlogoff) ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Profile URL</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <a href="<?= $info->profileurl; ?>" target="_blank"><?= $info->profileurl; ?></a>
                        </td>
                    </tr>

                    <!--The full URL of the player's 32x32px avatar. If the user hasn't configured an avatar, this will be the default ? avatar.-->
                    <!--<tr>
                        <td>Avatar</td>
                        <td class="mdl-data-table__cell--non-numeric"><img src="<?/*= $info->avatar; */?>" alt="Avatar"/></td>
                    </tr>-->
                    <!--The full URL of the player's 64x64px avatar. If the user hasn't configured an avatar, this will be the default ? avatar.-->
                    <!--<tr>
                        <td>Avatar Medium</td>
                        <td class="mdl-data-table__cell--non-numeric"><img src="<?/*= $info->avatarmedium; */?>" alt="Avatar Medium"/></td>
                    </tr>-->

                    <tr>
                        <td>Avatar Image</td>
                        <!--The full URL of the player's 184x184px avatar. If the user hasn't configured an avatar, this will be the default ? avatar.-->
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php
                            $check = [
                                'src' => $info->avatarfull,
                                'alt' => 'Avatar Full'
                            ];
                            echo img($check);
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Personal State</td>
                        <!--The user's current status. 0 - Offline, 1 - Online, 2 - Busy, 3 - Away, 4 - Snooze, 5 - looking to trade, 6 - looking to play.
                         If the player's profile is private, this will always be "0",
                         except if the user has set his status to looking to trade or looking to play,
                        because a bug makes those status appear even if the profile is private.-->
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php
                            switch ($info->personastate) {
                                case 0:
                                    echo 'Offline';
                                    break;
                                case 1:
                                    echo 'Online';
                                    break;
                                case 2:
                                    echo 'Busy';
                                    break;
                                case 3:
                                    echo 'Away';
                                    break;
                                case 4:
                                    echo 'Snooze';
                                    break;
                                case 5:
                                    echo 'Looking to Trade';
                                    break;
                                case 6:
                                    echo 'Looking to Play';
                                    break;
                            }
                            ?>
                        </td>
                    </tr>

                    <?php if(isset($info->commentpermission)): ?>
                        <tr>
                            <td>Comment Permission</td>
                            <!--If set, indicates the profile allows public comments.-->
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->commentpermission; ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(isset($info->realname)): ?>
                        <tr>
                            <td>Real Name</td>
                            <!--The player's "Real Name", if they have set it.-->
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->realname; ?></td>
                        </tr>
                    <?php endif; if(isset($info->primaryclanid)): ?>
                        <tr>
                            <td>Primary Clan ID</td>
                            <!--The player's primary group, as configured in their Steam Community profile.-->
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->primaryclanid; ?></td>
                        </tr>
                    <?php endif; if(isset($info->timecreated)): ?>
                        <tr>
                            <td>Account Created</td>
                            <!--The time the player's account was created.-->
                            <td class="mdl-data-table__cell--non-numeric"><?= gmdate("F j, Y, g:i a", $info->timecreated); ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Personal State Flags</td>
                        <td class="mdl-data-table__cell--non-numeric"><?=  $info->personastateflags; ?></td>
                    </tr>
                    <tr>
                        <td>Local Country Code</td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $country; ?>
                        </td>
                    </tr>
                    <?php if(isset($info->locstatecode)): ?>
                        <tr>
                            <td>Loc State Code</td>
                            <!--If set on the user's Steam Community profile, The user's state of residence-->
                            <td class="mdl-data-table__cell--non-numeric"><?=  $info->locstatecode; ?></td>
                        </tr>
                    <?php endif; if(isset($info->loccityid)):?>
                        <tr>
                            <td>Loc City ID</td>
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->loccityid; ?></td>
                        </tr>

                    <?php endif; if(isset($info->gameid)): ?>
                        <tr>
                            <td>game ID</td>
                            <!--If the user is currently in-game, this value will be returned and set to the gameid of that game.-->
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->gameid; ?></td>
                        </tr>
                    <?php endif; if(isset($info->gameserverip)): ?>
                        <tr>
                            <td>Game Server IP</td>
                            <!--The ip and port of the game server the user is currently playing on, if they are playing on-line in a game using Steam matchmaking.
                            Otherwise will be set to "0.0.0.0:0".-->
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->gameserverip; ?></td>
                        </tr>
                    <?php endif; if(isset($info->gameextrainfo)): ?>
                        <tr>
                            <td>Game Extra Info</td>
                            <td class="mdl-data-table__cell--non-numeric"><?= $info->gameextrainfo; ?></td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/info/'+id;
        }
    });
</script>