<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if(!count($userstats)): ?>
        <h1><?= $msg; ?></h1>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?= heading('Basic Stats', 3); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Steam ID</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $userstats->steamID ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Game Name</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $userstats->gameName ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Total Kills</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $statistics['total_kills']; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Total Kills Enemy Blinded</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $statistics['total_kills_enemy_blinded']; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Total Kills Knife Fight</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $statistics['total_kills_knife_fight']; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Total Kills Against Zoomed Sniper</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $statistics['total_kills_against_zoomed_sniper']; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Total Kills By Enemy Weapon</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $statistics['total_kills_enemy_weapon']; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">Total Death</td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $statistics['total_deaths']; ?></td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            K/D Ratio
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= number_format($statistics['total_kills']/$statistics['total_deaths'], 2, '.', ''); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Headshot %
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= number_format(($statistics['total_kills_headshot']/$statistics['total_kills'])*100, 2, '.', '').' %' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Accuracy %
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= number_format(($statistics['total_shots_hit']/$statistics['total_shots_fired'])*100, 2, '.', '').' %' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Wins
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_wins'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Wins Pistolround
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_wins_pistolround'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Rounds Played
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_rounds_played'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Win %
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= number_format(($statistics['total_wins']/$statistics['total_rounds_played'])*100, 2, '.', '').' %' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Mvps
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_mvps'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Planted Bombs
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_planted_bombs'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Defused Bombs
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_defused_bombs'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Damage Done
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_damage_done'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Money Earned
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= '$'.$statistics['total_money_earned'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Rescued Hostages
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_rescued_hostages'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Weapons Donated
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_weapons_donated'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Weapon Stats
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor(base_url('csgo/weaponstat/'.$userstats->steamID), 'See Weapon Stats', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Map Stats
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor(base_url('csgo/mapstat/'.$userstats->steamID), 'See Maps Statistics', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Operation Stats
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?php $url = explode('/', $_SESSION['url']); ?>
                            <?= anchor(base_url('csgo/operationstat/'.$url[3].'/'.$url[4]), 'See Operation Statistics', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Achievements
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= anchor(base_url('csgo/achievements/'.$userstats->steamID), 'See Achievements', $att = ['target' => '_blank']); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Broken Windows
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_broken_windows'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Dominations
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_dominations'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Domination Overkills
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_domination_overkills'] ?>
                        </td>
                    </tr>
                    <?php if (isset($statistics['total_revenges'])): ?>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Revenges
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_revenges'] ?>
                        </td>
                    </tr>
            <?php endif; ?>
                    <?php if (isset($statistics['total_gun_game_contribution_score'])): ?>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Total Gun Game Contribution Score
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['total_gun_game_contribution_score'] ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if (isset($statistics['last_match_gg_contribution_score'])): ?>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                            Last Match Gg Contribution Score
                        </td>
                        <td class="mdl-data-table__cell--non-numeric">
                            <?= $statistics['last_match_gg_contribution_score'] ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div>

        <!--<div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?/*= heading('Stats', 3); */?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <?php /*foreach($userstats->stats as $i =>$stats): */?>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric text-capitalize">
                                <?php /*$name = str_replace('_', ' ', $stats->name); echo $i.' '.$name; */?>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?/*= $stats->value */?>
                            </td>
                        </tr>
                    <?php /*endforeach; */?>
                </table>
                <?/*= heading('Achievements', 3); */?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <?php /*foreach($userstats->achievements as $achieve): */?>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric text-capitalize">
                                <?php /*$name = str_replace('_', ' ', $achieve->name); echo $name; */?>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?/*= $achieve->achieved */?>
                            </td>
                        </tr>
                    <?php /*endforeach; */?>
                </table>
            </div>
        </div>-->
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/game/userstats/730/'+id;
        }
    });
</script>