<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if (isset($msg)): ?>
        <div class="center">
            <?= heading($msg, 3); ?>
        </div>
    <?php elseif(!count($friends)): ?>
        <div class="center">
            <h2>No Friends found!!</h2>
        </div>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?= heading('Friend List', 3); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Steam ID</th>
                        <th class="mdl-data-table__cell--non-numeric">Steam Profile</th>
                        <th class="mdl-data-table__cell--non-numeric">Friend Since</th>
                    </tr>
                    </thead>
                    <?php foreach($friends as $f):?>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= $f->steamid; ?>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= anchor('http://steamcommunity.com/profiles/'.$f->steamid, 'http://steamcommunity.com/profiles/'.$f->steamid, $att = ['target' => '_blank']); ?>
                            </td>
                            <?php date_default_timezone_set('Asia/Dhaka'); ?>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= gmdate("F j, Y, g:i a", $f->friend_since); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/friends/'+id;
        }
    });
</script>