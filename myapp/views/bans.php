<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if (isset($msg)): ?>
        <div class="center">
            <?= heading($msg, 3); ?>
        </div>
    <?php elseif(!count($bans)): ?>
        <div class="center">
            <?= heading('No Record Found!', 3); ?>
        </div>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?= heading('Ban Status', 3); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <tr>
                        <td>
                            <b>Steam ID</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->SteamId; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Community Banned</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->CommunityBanned; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <b>VAC Banned</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->VACBanned; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Number Of VAC Bans</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->NumberOfVACBans; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Days Since Last Ban</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->DaysSinceLastBan; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Number Of Game Bans</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->NumberOfGameBans; ?></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Economy Ban</b>
                        </td>
                        <td class="mdl-data-table__cell--non-numeric"><?= $bans->EconomyBan; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/bans/'+id;
        }
    });
</script>