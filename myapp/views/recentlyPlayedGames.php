<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if (isset($msg)): ?>
        <div class="center">
            <?= heading($msg, 3); ?>
        </div>
    <?php elseif(!count($recPlayed)): ?>
        <div class="center">
            <?= heading('No Game Found!', 3); ?>
        </div>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?php if(!$recPlayed->total_count): ?>
                <h2>No Game played recently!!</h2>
                <?php else: ?>
                    <h2>Total Recently Played Games: <?= $recPlayed->total_count; ?></h2>
                    <?php foreach($recPlayed->games as $g): ?>
                        <table class="mdl-data-table mdl-js-data-table center">
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">App ID</td>
                                <td class="mdl-data-table__cell--non-numeric"><?= $g->appid; ?></td>
                            </tr>
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">Name</td>
                                <td class="mdl-data-table__cell--non-numeric"><?= $g->name; ?></td>
                            </tr>
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">Past 2 weeks playtime</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <?= floor($g->playtime_2weeks/60).' hr '. $g->playtime_2weeks % 60 .' min'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">Forever playtime</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <?= floor($g->playtime_forever/60).' hr '. $g->playtime_forever % 60 .' min'; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">Icon</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <?= img('http://cdn.akamai.steamstatic.com/steamcommunity/public/images/apps/730/'.$g->img_icon_url.'.jpg', $att=['alt' => 'Icon Image']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">Logo</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <?= img('http://cdn.akamai.steamstatic.com/steamcommunity/public/images/apps/730/'.$g->img_logo_url.'.jpg', $att=['alt' => 'Logo Image']); ?>
                                </td>
                            </tr>
                        </table>
                    <?php endforeach; endif; ?>
            </div>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/recentlyPlayedGames/'+id;
        }
    });
</script>