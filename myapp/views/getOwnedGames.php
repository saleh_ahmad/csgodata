<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <?php if (isset($msg)): ?>
        <div class="center">
            <?= heading($msg, 3); ?>
        </div>
    <?php elseif(!count($owned)): ?>
        <div class="center">
            <?= heading('No Game Found!', 3); ?>
        </div>
    <?php else: ?>
        <div class="mdl-grid center">
            <div class="mdl-cell mdl-cell--12-col">
                <?= heading('Total Owned Games: '.$owned->game_count, 3); ?>
                <table class="mdl-data-table mdl-js-data-table center">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Game ID</th>
                        <th class="mdl-data-table__cell--non-numeric">Game URL</th>
                        <th class="mdl-data-table__cell--non-numeric">Game Information</th>
                        <th class="mdl-data-table__cell--non-numeric">Playtime Forever</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($owned->games as $g): ?>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= $g->appid; ?>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= anchor(
                                    'http://store.steampowered.com/app/'.$g->appid,
                                    'http://store.steampowered.com/app/'.$g->appid,
                                    $attr= ['target' => '_blank']
                                ) ?>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= anchor(
                                    base_url('game/info/'.$g->appid),
                                    $g->appid,
                                    $att = ['target' => '_blank']
                                ); ?>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <?= floor($g->playtime_forever/60).' hr '. $g->playtime_forever % 60 .' min'; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</section>
<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/getOwnedGames/'+id;
        }
    });
</script>