<div class="mdl-grid">
    <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
        <div class="mdl-tabs__tab-bar">
            <?= anchor('#weapon-panel', 'All', $attr = ['class' => 'mdl-tabs__tab is-active weapon-stat', 'data-type' => 'all']) ?>
            <?= anchor('#weapon-panel', 'Pistols', $attr = ['class' => 'mdl-tabs__tab weapon-stat', 'data-type' => 'pistol']) ?>
            <?= anchor('#weapon-panel', 'Rifles', $attr = ['class' => 'mdl-tabs__tab weapon-stat', 'data-type' => 'rifle']) ?>
            <?= anchor('#weapon-panel', 'SMGs', $attr = ['class' => 'mdl-tabs__tab weapon-stat', 'data-type' => 'smg']) ?>
            <?= anchor('#weapon-panel', 'Heavy', $attr = ['class' => 'mdl-tabs__tab weapon-stat', 'data-type' => 'heavy']) ?>
            <?= anchor('#weapon-panel', 'Others', $attr = ['class' => 'mdl-tabs__tab weapon-stat', 'data-type' => 'others']) ?>
        </div>
    </div>

    <script>
        $('.weapon-stat').click(function (e) {
            e.preventDefault();
            var type = $(this).data('type');
            $.ajax({
                url: root() + '/csgo/weapon/' + type + '/<?= $_SESSION['steam64']; ?>',
                dataType: 'json',
                cache : false,
                success: function(data){
                    var content = '';
                    for (x in data.weapondata) {
                        content += '<div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--6-col-phone">' +
                                '<h4>' + x + '</h4>' +
                                '<div class="weapon-card">' +
                                    '<img src="' + root() + '/assets/img/weapon/' + x + '.png" class="width-100" alt="' + x + '" />' +
                                '</div>' +
                                '<table class="mdl-data-table mdl-js-data-table width-100">' +
                                '<tr>' +
                                    '<td class="mdl-data-table__cell--non-numeric">Total Kills</td>' +
                                    '<td class="mdl-data-table__cell--non-numeric">' + data.weapondata[x].kills + '<td>' +
                                '</tr>';

                        if ( (typeof(data.weapondata[x].hits) != "undefined" && data.weapondata[x].hits !== null)
                            && (typeof(data.weapondata[x].shots) != "undefined" && data.weapondata[x].shots !== null)
                            && (typeof(data.weapondata[x].accuracy) != "undefined" && data.weapondata[x].accuracy !== null) ) {
                            content += '<tr>' +
                                '<td class="mdl-data-table__cell--non-numeric">Total Hits</td>' +
                                '<td class="mdl-data-table__cell--non-numeric">' + data.weapondata[x].hits + '<td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td class="mdl-data-table__cell--non-numeric">Total Shots</td>' +
                                    '<td class="mdl-data-table__cell--non-numeric">' + data.weapondata[x].shots + '<td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td class="mdl-data-table__cell--non-numeric">Accuracy</td>' +
                                    '<td class="mdl-data-table__cell--non-numeric">' + Math.abs(parseFloat(data.weapondata[x].accuracy)).toFixed(2)  + '%<td>' +
                                '</tr>';
                        }

                        content += '</table></div>'
                    }

                    $('#weapon-panel').html(
                        '<div class="mdl-grid center">' + content + '</div>'
                    );
                }
            });
        });
    </script>
</div>