<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (isset($_SESSION['steam64'])): ?>
        </div>
    </main>
    <footer>
      <p>
          <?php if ((ENVIRONMENT === 'development')): ?>
            Page rendered in <strong>{elapsed_time}</strong> seconds. CodeIgniter Version <strong><?= CI_VERSION ?></strong>
          <?php endif; ?>
        Noob Lonely &copy; 2016
      </p>
    </footer>
</div><!-- ./demo-layout-waterfall ./mdl-layout ./mdl-js-layout -->
<?php endif; ?>
</body>
</html>