<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?= doctype('html5'); ?>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title><?= $title ?></title>

  <meta name="viewport"              content="width=device-width, initial-scale=1">
  <meta name="description"           content="It is a demo codeigniter project that can be used to check a steam user's information as well as CSGO information and statistics" />
  <meta name="keywords"              content="Lonely,L0onely,noob lonely,noob lonely's project,noob lonely's php project,noob,nooblonely,csgodata,csgo data,csgo information website,steam,steam information website,
PHP,PHP Project,google material design lite,material design Used Project,demo project,demo php project,php awesome projects,codeigniter,codeigniter projects,codeigniter with material design lite,get steam information by website,
get csgo information by website,csgo stats website,steam stats website,steam api used website" />
  <meta name="language"              content="English" />
  <meta name="country"               content="Bangladesh" />
  <meta name="Title"                 content="CSGO Data : <?= $title ?>" />
  <meta name="Owner"                 content="Noob L0onely" />
  <meta name="Copyright"             content="2016 CSGO Data all rights reserved" />
  <meta name="Author"                content="Saleh Ahmad" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robots"                content="index, nofollow">

  <!-- Canonical SEO -->
  <link rel="canonical"              href="http://www.nooblonely.com">

  <!-- Open Graph data -->
  <meta property="og:locale"         content="en_US" />
  <meta property="og:url"            content="<?= base_url() ?>" />
  <meta property="og:type"           content="website" />
  <meta property="og:title"          content="CSGO Data | Profile Information" />
  <meta property="og:site_name"      content="CSGO Data"
  <meta property="og:description"    content="It is a demo codeigniter project that can be used to check a steam user's information as well as CSGO information and statistics" />
  <meta property="og:image"          content="<?= base_url('csgodata.png') ?>" />

  <!-- Twitter Card data -->
  <meta name="twitter:card"          content="Course_Management" />
  <meta name="twitter:description"   content="It is a demo codeigniter project that can be used to check a steam user's information as well as CSGO information and statistics" />
  <meta name="twitter:title"         content="CSGO Data | Profile Information" />
  <meta name="twitter:site"          content="@Nissongo_10" />
  <meta name="twitter:image"         content="<?= base_url('csgodata.png') ?>" />
  <meta name="twitter:creator"       content="@Nissongo_10" />

  <!-- favicons -->
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= base_url('favicons/apple-touch-icon-57x57.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url('favicons/apple-touch-icon-114x114.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url('favicons/apple-touch-icon-72x72.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url('favicons/apple-touch-icon-144x144.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= base_url('favicons/apple-touch-icon-60x60.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= base_url('favicons/apple-touch-icon-120x120.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= base_url('favicons/apple-touch-icon-76x76.png') ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= base_url('favicons/apple-touch-icon-152x152.png') ?>" />
  <link rel="icon" type="image/png" href="<?= base_url('favicons/favicon-196x196.png') ?>" sizes="196x196" />
  <link rel="icon" type="image/png" href="<?= base_url('favicons/favicon-96x96.png') ?>" sizes="96x96" />
  <link rel="icon" type="image/png" href="<?= base_url('favicons/favicon-32x32.png') ?>" sizes="32x32" />
  <link rel="icon" type="image/png" href="<?= base_url('favicons/favicon-16x16.png') ?>" sizes="16x16" />
  <link rel="icon" type="image/png" href="<?= base_url('favicons/favicon-128.png') ?>" sizes="128x128" />
  <meta name="application-name"                content="&nbsp;"/>
  <meta name="msapplication-TileColor"         content="#FFFFFF" />
  <meta name="msapplication-TileImage"         content="<?= base_url('favicons/mstile-144x144.png') ?>" />
  <meta name="msapplication-square70x70logo"   content="<?= base_url('favicons/mstile-70x70.png') ?>" />
  <meta name="msapplication-square150x150logo" content="<?= base_url('favicons/mstile-150x150.png') ?>" />
  <meta name="msapplication-wide310x150logo"   content="<?= base_url('favicons/mstile-310x150.png') ?>" />
  <meta name="msapplication-square310x310logo" content="<?= base_url('favicons/mstile-310x310.png') ?>" />

  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-pink.min.css">
  <script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>
  <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>" />

  <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="<?= base_url('assets/js/jquery-3.1.1.min.js') ?>">\x3C/script>')</script>

  <script src="<?= base_url('assets/js/myscript.js') ?>"></script>
</head>
<body>

<?php if (isset($_SESSION['steam64'])): ?>
<div class="demo-layout-waterfall mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--waterfall">
    <div class="mdl-layout__header-row">
      <span class="mdl-layout-title">
          <?php
          $logo = [
              'src'   => base_url('csgodata.png'),
              'alt'   => 'CSGO Data Logo',
              'width' => '120px',
              'style' => 'margin-top:60px'
          ];
          echo anchor(base_url(), img($logo)); ?>
      </span>
      <div class="mdl-layout-spacer"></div>
        <?php if ($search): ?>
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right">
            <label class="mdl-button mdl-js-button mdl-button--icon"
                   for="waterfall-exp">
              <i class="material-icons">search</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input searchId"
                     type="search"
                     name="sample"
                     id="waterfall-exp"
                     placeholder="<?= $searchtext ?>">
            </div>
          </div>
        <?php endif; ?>
    </div>
        <div class="mdl-layout__header-row">
            <div class="mdl-layout-spacer"></div>
            <nav class="mdl-navigation">
                <?php
                $atts = [
                    'class'  => 'mdl-navigation__link'
                ];
                $url = explode('/', $_SESSION['url']);

                $avatar = [
                    'src' => $_SESSION['avatar'],
                    'alt' => 'User Avatar'
                ];

                echo anchor(base_url(), img($avatar), $atts);
                echo anchor(base_url('info/'.$_SESSION['steam64']), '<i class="material-icons">info_outline</i> Basic Information', $atts);
                echo anchor(base_url('friends/'.$_SESSION['steam64']), '<i class="material-icons">group</i> Friends', $atts);
                echo anchor(base_url('getOwnedGames/'.$_SESSION['steam64']), '<i class="material-icons">attach_money</i> Owned game', $atts);
                echo anchor(base_url('recentlyPlayedGames/'.$_SESSION['steam64']), '<i class="material-icons">alarm_on</i> Recently Played Games', $atts);
                echo anchor(base_url('bans/'.$_SESSION['steam64']), '<i class="material-icons">do_not_disturb</i> Bans', $atts);
                echo anchor(base_url('items/'.$url[3].'/'.$url[4]), '<i class="material-icons">shopping_basket</i> Inventory Items', $atts);
                echo anchor(base_url('logout'), '<i class="material-icons">power_settings_new</i> Logout', $atts);
                ?>
            </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">
            <?php
            $attr = [
                'class' => 'drawer-title-font'
            ];
            ?>
            <?= anchor(base_url(), img($avatar). str_repeat('&nbsp', 2) .$_SESSION['personaname'], $attr); ?>
        </span>
        <hr />
        <nav class="mdl-navigation">
            <?php
            echo anchor(base_url('info/'.$_SESSION['steam64']), '<i class="material-icons">info_outline</i> Basic Information', $atts);
            echo anchor(base_url('friends/'.$_SESSION['steam64']), '<i class="material-icons">group</i> Friends', $atts);
            echo anchor(base_url('getOwnedGames/'.$_SESSION['steam64']), '<i class="material-icons">attach_money</i> Owned game', $atts);
            echo anchor(base_url('recentlyPlayedGames/'.$_SESSION['steam64']), '<i class="material-icons">alarm_on</i> Recently Played Games', $atts);
            echo anchor(base_url('bans/'.$_SESSION['steam64']), '<i class="material-icons">do_not_disturb</i> Bans', $atts);
            echo anchor(base_url('logout'), '<i class="material-icons">power_settings_new</i> Logout', $atts);
            ?>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content">

    <?php
    $logo = [
        'src'   => base_url('csgodata.png'),
        'alt'   => 'CSGO Data Logo',
        'width' => '150px'
    ];
    ?>
    <div class="center" style="display: none" id="logo-area">
        <?= anchor(base_url(), img($logo)); ?>
    </div>

<?php  endif; ?>