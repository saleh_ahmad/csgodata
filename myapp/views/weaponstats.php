<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .cover-image-weapon{
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        z-index: -1;
        background-image: url(<?= base_url('assets/img/weapon-background.jpg') ?>);
        background-position: center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        filter: blur(3px);
        height: 94.4%;
        width: 99%;
    }
</style>
<section>
    <div class="cover-image-weapon"></div>
    <?php if(!count($weaponstats)): ?>
        <h1><?= $msg; ?></h1>
    <?php else: ?>
    <div class="center">
        <?= heading($title, 3); ?>
    </div>

    <?php include_once 'templates/weaponstatmenu.php';?>
        <div class="mdl-tabs__panel" id="weapon-panel">
            <div class="mdl-grid center">
                <?php foreach($weapondata as $i => $data): ?>
                    <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--6-col-phone">
                        <?= heading($i, 4); ?>
                        <div class="weapon-card">
                            <?= img([
                                'src'   => base_url('assets/img/weapon/'.$i.'.png'),
                                'class' => 'width-100',
                                'alt'   => $i
                            ]); ?>
                        </div>
                        <table class="mdl-data-table mdl-js-data-table width-100">
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">Total Kills</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <?= $data['kills'] ?>
                                </td>
                            </tr>
                            <?php if(isset($data['hits']) && isset($data['shots']) && isset($data['accuracy'])): ?>
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">Total Hits</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <?= $data['hits'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        Total Shots
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <?= $data['shots'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">Accuracy</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <?= number_format($data['accuracy'], 2, '.', '').' %' ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</section>
<!--<script>
    $('.searchId').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).val();
            window.location = root()+'/csgo/weaponstat/<?/*= $type; */?>/'+id;
        }
    });
</script>-->