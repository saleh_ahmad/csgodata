<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
    <div class="mdl-grid center">
        <div class="mdl-cell mdl-cell--12-col">
            <h2>Current Market Situation:</h2>
            <table class="mdl-data-table mdl-js-data-table center">
                <tbody>
                <tr>
                    <td>Lowest Price</td>
                    <td><?= $details->lowest_price; ?></td>
                </tr>
                <tr>
                    <td>Volume</td>
                    <td><?= $details->volume; ?></td>
                </tr>
                <tr>
                    <td>Median Price</td>
                    <td><?= $details->median_price; ?></td>
                </tr>
                </tbody>
            </table>
            <h2>Last 7 Days Market Situation:</h2>
            <table class="mdl-data-table mdl-js-data-table center">
                <tr>
                    <td>Average Price</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->average_price; ?></td>
                </tr>
                <tr>
                    <td>Median Price</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->median_price; ?></td>
                </tr>
                <tr>
                    <td>Amount Sold</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->amount_sold; ?></td>
                </tr>
                <tr>
                    <td>Standard Deviation</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->standard_deviation; ?></td>
                </tr>
                <tr>
                    <td>Lowest Price</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->lowest_price; ?></td>
                </tr>
                <tr>
                    <td>Highest Price</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->highest_price; ?></td>
                </tr>
                <tr>
                    <td>First Sale Date</td>
                    <?php date_default_timezone_set('Asia/Dhaka'); ?>
                    <td class="mdl-data-table__cell--non-numeric"><?= gmdate("F j, Y, g:i a", $stat['details']->first_sale_date); ?></td>
                </tr>
                <tr>
                    <td>icon</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <img src="<?= $stat['details']->icon; ?>" alt="icon"/>
                    </td>
                </tr>
                <tr>
                    <td>Currency</td>
                    <td class="mdl-data-table__cell--non-numeric"><?= $stat['details']->currency; ?></td>
                </tr>
            </table>
        </div>
    </div>
</section>