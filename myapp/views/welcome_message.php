<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .cover-image{
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        z-index: -1;
        background-image: url(<?= base_url('assets/img/landing-background.jpg') ?>);
        background-position: center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        height: 100%;
    }
</style>
<section>
    <div class="cover-image"></div>
    <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--12-col">
            <?= form_open('?login'); ?>
            <div class="center" id="steam-login-btn">
                <input type="image" src="assets/img/sits_02.png">
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</section>